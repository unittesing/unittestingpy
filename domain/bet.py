class Bet:
    amount = 0
    score = 0

    def __init__(self, amount, score):
        self.amount = amount
        self.score = score

    def get_amount(self):
        return self.amount

    def get_score(self):
        return self.score