from random import random


class RollDiceGame(object):
    bets = dict({})

    def add_bet(self, player, bet):
        self.bets[player] = bet

    def play(self):
        winning_score = random.randint(1, 6)

        for player in self.bets:
            if self.bets[player].get_score == winning_score:
                player.win(self.bets[player].get_amount * 6)
            else:
                player.lose()

        self.bets.clear()

    def leave(self, player):
        if not self.bets.keys().__contains__(player):
            return

        del self.bets[player]