from domain.exception import CasinoGameException


class Player:
    activeGame = None
    chips = 0

    def active_game(self):
        return self.activeGame

    def _is_in_game(self):
        return self.activeGame is not None

    def get_available_chips(self):
        return self.chips

    def join(self, game):
        if self._is_in_game():
            raise CasinoGameException("Player must leave the current game before joining another game")
        self.activeGame = game

    def leave(self):
        self.activeGame.leave(self)
        self.activeGame = None

    def buy(self, chips):
        if chips < 0:
            raise CasinoGameException("Buying negative numbers is not allowed")
        self.chips = chips

    def bet(self, bet):
        if bet.get_amount() > self.chips:
            raise CasinoGameException("Can not bet more than chips available")

        self.chips = self.chips - bet.get_amount()
        self.activeGame.add_bet(self, bet)

    def win(self, chips):
        self.chips = self.chips + chips

    def lose(self):
        pass
