# Roll Dice Game

simple domain for Unit testing workshop

## Setup environment
```
python3 -m venv venv
source venv/bin/activate
```

## To run tests
```
python3 -m unittest
```